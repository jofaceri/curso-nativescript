import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { registerElement } from "@nativescript/angular";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Application } from "@nativescript/core";
import { device, screen } from "tns-core-modules/platform";
import { 
    connectionType,
    getConnectionType,
    startMonitoring,
    stopMonitoring
    } from "tns-core-modules/connectivity";

registerElement("MapView", () => require("nativescript-google-maps-sdk").MapView);

@Component({
    selector: "Browse",
    templateUrl: "./browse.component.html"
})
export class BrowseComponent implements OnInit {
    @ViewChild("MapView") mapView: ElementRef;
    

    constructor() {
        // Use the component constructor to inject providers.
        var gmaps = require("nativescript-google-maps-sdk");

        function onMapReady(args) {
            var mapView = args.object;
            var marker = new gmaps.Marker();
            marker.position = gmaps.Position.positionFromLatLng(-34.6037, -58.3817);
            marker.title = "Buenos Aires";
            marker.snippet = "Argentina";
            marker.userData = { index : 1 };
            mapView.addMarker(marker);
        }
    }

    ngOnInit(): void {
        // Init your component properties here.
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>Application.getRootView();
        sideDrawer.showDrawer();
    }
    
    // Map events
    /*onMapReady(event): void {
        console.log("Map ready");
    }*/

    onDatosPlataforma(): void {
        console.log("modelo", device.model);
        console.log("tipo dispositivo", device.deviceType);
        console.log("Sistema Operativo", device.os);
        console.log("versión sistema operativo", device.osVersion);
        console.log("Versión SDK", device.sdkVersion);
        console.log("Lenguaje", device.language);
        console.log("fabricante", device.manufacturer);
        console.log("Código único de dispositivo", device.uuid);
        console.log("altura en pixels normalizados", screen.mainScreen.heightDIPs);//DIP (Device Independent Pixel)
        console.log("altura pixels", screen.mainScreen.heightPixels);
        console.log("escala pantalla", screen.mainScreen.scale);
        console.log("ancho pixels normalizados", screen.mainScreen.widthDIPs);
        console.log("ancho pixels", screen.mainScreen.widthPixels);
    }

    monitoreando: boolean = false; // una variable para saber si está monitoreando o no

    onMonitoreoDatos(): void {
        const myConnectionType = getConnectionType();

        switch (myConnectionType) {
            case connectionType.none:
                console.log("Sin conexión");
                break;
            case connectionType.wifi:
                console.log("WiFi");
                break;
            case connectionType.mobile:
                console.log("Mobile");
                break;
            case connectionType.ethernet:
                console.log("Ethernet");
                break;
            case connectionType.bluetooth:
                console.log("Bluetooth");
                break;
            default:
                break;
        }

        this.monitoreando = !this.monitoreando;

        if (this.monitoreando) {
            startMonitoring((newConnectionType) => {
                switch (newConnectionType) {
                    case connectionType.none:
                        console.log("Cambió a sin conexión");
                        break;
                    case connectionType.wifi:
                        console.log("Cambió a WiFi");
                        break;
                    case connectionType.mobile:
                        console.log("Cambió a mobile");
                        break;
                    case connectionType.ethernet:
                        console.log("Cambió a Ethernet");
                        break;
                    case connectionType.bluetooth:
                        console.log("Cambió a bluetooth");
                        break;
                    default:
                        break;
                }
            });
        } else {
            stopMonitoring();
        }
    }
}
