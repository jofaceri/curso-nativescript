import { Injectable } from '@angular/core';
import { Couchbase, ConcurrencyMode } from 'nativescript-couchbase-plugin';
import { getJSON, request } from "tns-core-modules/http";

const sqlite = require("nativescript-sqlite");
const database = new Couchbase('my-database');

@Injectable()
export class NoticiasService {
    //private noticias: Array<string> = [];
    api: string = "https://0c8fa861a2fe.ngrok.io";
    
    constructor() {
        this.getDb((db) => {
            console.dir(db);
            db.each("select * from logs",
                (err, fila) => console.log("fila: ", fila),
                (err, totales) => console.log("Filas totales: ", totales));
        }, () => console.log("error on getDB"));

        database.createView("logs", "1", (document, emitter) =>
            emitter.emit(document._id, document));
        const rows = database.executeQuery("logs", {limit : 200});
        console.log("documentos: " + JSON.stringify(rows));
    }

    getDb(fnOk, fnError) {
        return new sqlite("mi_db_logs", (err, db) => {
            if (err) {
                console.error("Error al abrir db!", err);
            } else {
                console.log("Está la db abierta: ", db.isOpen() ? "Si" : "No");
                db.execSQL("CREATE TABLE IF NOT EXISTS logs (id INTEGER PRIMARY KEY AUTOINCREMENT, texto TEXT")
                    .then((id) => {
                        console.log("CREATE TABLE OK");
                        fnOk(db);
                    }, (error) => {
                        console.log("CREATE TABLE ERROR", error);
                        fnError(error);
                    });
            }
        });
    }

    agregar(s: string) {
        //this.noticias.push(s);
        return request({
            url: this.api + "/favs",
            method: "POST",
            headers: { "Content-Type": "application/json" },
            content: JSON.stringify({
                nuevo: s
            })
        });
    }

    favs() {
        return getJSON(this.api + "/favs");
    }

    buscar(s: string) {
        //return this.noticias;
        this.getDb((db) => {
            db.execSQL("insert into logs (texto) values (?)", [s],
                (err, id) => console.log("nuevo id:", id));
        }, () => console.log("error on getDB"));

        const documentId = database.createDocument({ texto: s });
        console.log("nuevo id couchbase: ", documentId);
        
        return getJSON(this.api + "/get?q=" + s);
    }
}