import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { compose } from "nativescript-email";
import { Store } from "@ngrx/store";
import * as SocialShare from "nativescript-social-share";
import * as Toast from "nativescript-toasts";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Application, Color, Dialogs, View } from "@nativescript/core";
import { NoticiasService } from "../domain/noticias.service";
import { registerElement } from "@nativescript/angular";
import { delay } from "rxjs/operators";
import { AppState } from "../app.module";
import { Noticia, NuevaNoticiaAction } from "../domain/noticias-state.model";
import { ConstantPool } from "@angular/compiler";

registerElement("PullToRefresh", () => require("@nstudio/nativescript-pulltorefresh").PullToRefresh);

@Component({
    selector: "Search",
    templateUrl: "./search.component.html",
    //providers: [NoticiasService]
})

export class SearchComponent implements OnInit {
    resultados: Array<string>;
    @ViewChild("layout") layout: ElementRef;

    constructor(
        public noticias: NoticiasService,
        public store: Store<AppState>) {
        // Use the component constructor to inject providers.
    }

    doLater(fn) { setTimeout(fn, 1000);}

    ngOnInit(): void {
        // Init your component properties here.
        /*this.noticias.agregar("Hola!");
        this.noticias.agregar("Seleccionar categoría");
        this.noticias.agregar("Opcional");
        this.doLater(() =>
            Dialogs.action("Mensaje", "Ingresar", ["Seleccionar"])
                .then((result) => {
                    console.log("resultado" + result);
                    if (result === "Seleccionar") {
                        //this.doLater(() => 
                            const toastOptions: Toast.ToastOptions = {text: "Atributo seleccionado", duration: Toast.DURATION.SHORT}
                            this.doLater(() => Toast.show(toastOptions));
                    } /*else if (result === "Opcion2") {
                        this.doLater(() =>
                            Dialogs.alert({
                                title: "Título 2",
                                message: "msg 2",
                                okButtonText: "btn 2"
                            }).then(() => console.log("Cerrado 2!")));
                    }
                }));*/
        this.store.select((state) => state.noticias.sugerida)
            .subscribe((data) => {
                const f = data;
                if (f != null) {
                    Toast.show({text: "Sugerimos leer: " + f.titulo, duration: Toast.DURATION.SHORT});
                }
            });
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>Application.getRootView();
        sideDrawer.showDrawer();
    }

    onItemTap(args): void {
        //console.dir(x);
        this.store.dispatch(new NuevaNoticiaAction(new Noticia(args.view.bindingContext)));
    }

    onLongPress(s): void {
        console.log(s);
        SocialShare.shareText(s, "Asunto: compartido desde el curso!");
    }

    refreshList(args) {
        const pullRefresh = args.object;
        setTimeout(function () {
           pullRefresh.refreshing = false;
        }, 1000);
    }

    buscarAhora(s: string) {
        /*this.resultados = this.noticias.buscar().filter((x) => x.indexOf(s) >= 0);

        const layout = <View>this.layout.nativeElement;
        layout.animate({
            backgroundColor: new Color("blue"),
            duration: 3000,
            delay: 1500
        }).then(() => layout.animate({
            backgroundColor: new Color("white"),
            duration: 3000,
            delay: 1500
        }));*/
        console.dir("buscarAhora" + s);
        this.noticias.buscar(s).then((r: any) => {
            console.log("resultados buscarAhora: " + JSON.stringify(r));
            this.resultados = r;
        }, (e) => {
            console.log("error buscarAhora " + e);
            Toast.show({text: "Error en la búsqueda", duration: Toast.DURATION.SHORT});
        });
    }

    envioEmail() {
        const fs = require("file-system"); // usas el filesystem para adjuntar un archivo
        const appFolder = fs.knownFolders.currentApp(); // esto te da un objeto de tipo Folder
        const appPath = appFolder.path; // esto te da el path a la carpeta src
        const logoPath = appPath + "/app/res/icon.png"; // aquí armas el path del archivo copiado
        compose({
            subject: "Mail de Prueba", // asunto del mail
            body: "Hola <strong>mundo!</strong> :)", // cuerpo que será enviado
            to: ["mail@mail.com"], //lista de destinatarios principales
            cc: [], //lista de destinatarios en copia
            bcc: [], //lista de destinatarios en copia oculta
            attachments: [ //listado de archivos adjuntos
                {fileName: "arrow1.png", // este archivo adjunto está en formato base 64 representado por un string
                    path: "base64://iVBORw0KGgoAAAANSUhEUgAAABYAAAAoCAYAAAD6xArmAAAACXBIWXMAABYlAAAWJQFJUiTwAAAAHGlET1QAAAACAAAAAAAAABQAAAAoAAAAFAAAABQAAAB5EsHiAAAAAEVJREFUSA1iYKAimDhxYjwIU9FIBgaQgZMmTfoPwlOmTJGniuHIhlLNxaOGwiNqNEypkwlGk9RokoIUfaM5ijo5Clh9AAAAAP//ksWFvgAAAEFJREFUY5g4cWL8pEmT/oMwiM1ATTBqONbQHA2W0WDBGgJYBUdTy2iwYA0BrILDI7VMmTJFHqv3yBUEBQsIg/QDAJNpcv6v+k1ZAAAAAElFTkSuQmCC",
                    mimeType: "image/png" 
                },
                {fileName: "icon.png", // este archivo es el que lees directo del filesystem del mobile
                    path: logoPath,
                    mimeType: "image/png"
                }]
        }).then(() => console.log("Enviador de mail cerrado"), (err) => console.log("Error: " + err));
    }
}
