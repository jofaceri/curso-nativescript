import { Component, OnInit } from "@angular/core";
import * as camera from "nativescript-camera";
import { Image } from "tns-core-modules/ui/image";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Application } from "@nativescript/core";
import { NoticiasService } from "../domain/noticias.service";
import * as imageSourceModule from "image-source";

@Component({
    selector: "Home",
    templateUrl: "./home.component.html"
})
export class HomeComponent implements OnInit {

    constructor(public noticias:NoticiasService) {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        // Init your component properties here.
        this.noticias.agregar("Home 1");
        this.noticias.agregar("Home 2");
        this.noticias.agregar("Home 3");
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>Application.getRootView();
        sideDrawer.showDrawer();
    }

    onButtonTap(): void {
        camera.requestPermissions().then(
            function success() {
                const options = { width: 300, height: 300, keepAspectRatio: false, saveToGallery: true};
                camera.takePicture(options).
                    then((ImageAsset) => {
                        console.log("Tamaño: " + ImageAsset.options.width + "x" + ImageAsset.options.height);
                        console.log("keepAspectRatio: " + ImageAsset.options.keepAspectRatio);
                        console.log("Foto guardada!");
                    }).catch((err) => {
                        console.log("Error -> " + err.message);
                    });
                camera.takePicture(options).
                    then((imageAsset) => {
                        imageSourceModule.fromAsset(imageAsset)
                            .then((imageSource) => {
                                SocialShare.shareImage(imageSource, "Asunto: compartido desde el curso!");
                            }).catch((err) => {
                                console.log("Error -> " + err.message);
                            });
                    }).catch((err) => {
                        console.log("Error -> " + err.message);
                        });
            },
            function failure() {
                console.log("Permiso de cámara no aceptado por el usuario");
            }
        );
    }
}
